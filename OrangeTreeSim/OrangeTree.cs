﻿using System;
using System.Threading;

namespace OrangeTreeSim
{


    public class OrangeTree
    {
        private int age = 0;
        private int height;
        private bool treeAlive;
        private int numOranges;
        private int orangesEaten;




        public int Age
        {
            set 
            { age = value;
                if (value >= 0)
                {
                    this.age = value;
                }
                else
                {
                    value = 0;
                }
            }
            get { return age; }
        }

        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        public bool TreeAlive
        {
            get { return treeAlive; }
            set { treeAlive = value; }
        }

        public int NumOranges
        {
            get { return numOranges; }
        }

        public int OrangesEaten
        {
            get { return orangesEaten; }
        }


        public void OneYearPasses()
        {
            age++;
            orangesEaten = 0;
            if (Age < 80)
            {

                height = height + 2;

                if (Age >= 2)
                {
                    numOranges = (Age - 1) * 5;
                }

            }
            else
            {
                treeAlive = false;
                numOranges = 0;
            }

        }

        public int EatOrange(int count)
        {

            orangesEaten = orangesEaten + count;
            return count;
        }
        
    }
}
